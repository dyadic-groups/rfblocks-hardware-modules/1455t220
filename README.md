# RF Blocks Module Carrier Board for Hammond 1455T220 Enclosures

## Documentation

Documentation for the carrier board is available at 
[RF Blocks](https://rfblocks.org/articles/RF-Blocks.html#module-carrier-boards)

## License

[CERN-OHL-W v2.](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2)
